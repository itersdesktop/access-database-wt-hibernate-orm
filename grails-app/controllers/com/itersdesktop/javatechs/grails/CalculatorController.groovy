package com.itersdesktop.javatechs.grails

import org.slf4j.Logger
import org.slf4j.LoggerFactory

class CalculatorController {
    private static final Logger logger = LoggerFactory.getLogger(this.getClass().getName())

    static allowedMethods = [save: 'POST']

    def index() {
        render(view: "index")
    }
    def save() {
        String message = "User agent: " + request.getHeader("User-Agent")
        logger.debug(message)
        println(message)
        withForm {
            // take data and persist
            render "Saved data successfully"
        }.invalidToken {
            //response.status = 405
            render(status: 405, text: "Invalid Token because you might make this request from outside of our website")
        }
    }
}
