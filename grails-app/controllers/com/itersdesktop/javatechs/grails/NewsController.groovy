package com.itersdesktop.javatechs.grails

class NewsController {
    def newsService

    def index() {
        [news: newsService.allNews, title: "All News | Grails App"]
    }
}
