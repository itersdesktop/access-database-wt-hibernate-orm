package com.itersdesktop.javatechs.grails

import org.apache.juli.logging.LogFactory

class ArticleController {
    private static final logger = LogFactory.getLog(this)
    def articleService

    def index() {
        logger.debug("Accessed the index page")
        def articles = articleService.articles
        int nbArticles = articles?.size()
        [nbArticles: nbArticles]
    }
}
