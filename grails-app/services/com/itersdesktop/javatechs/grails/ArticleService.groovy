package com.itersdesktop.javatechs.grails

import grails.transaction.Transactional
import org.apache.commons.logging.LogFactory

@Transactional
class ArticleService {
    private static final logger = LogFactory.getLog(ArticleService.class)
    // Auto inject SessionFactory we can use
    // to get the current Hibernate session.
    def sessionFactory

    List getArticles() {

        // Get the current Hibernate session.
        final session = sessionFactory.currentSession

        // Query string with :startId as parameter placeholder.
        final String query = 'select id, title, year from article order by title desc'

        // Create native SQL query.
        final sqlQuery = session.createSQLQuery(query)

        // Use Groovy with() method to invoke multiple methods
        // on the sqlQuery object.
        final results = sqlQuery.with {
            // Set domain class as entity.
            // Properties in domain class id, name, level will
            // be automatically filled.
//            addEntity(Organisation)

            // Set value for parameter startId.
//            setLong('startId', startOrganisationId)

            // Get all results.
            list()
        }
        results
    }
}
