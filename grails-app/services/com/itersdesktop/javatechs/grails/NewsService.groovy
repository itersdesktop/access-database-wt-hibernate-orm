package com.itersdesktop.javatechs.grails

import grails.transaction.Transactional
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import java.sql.Connection
import java.sql.DriverManager
import java.sql.ResultSet
import java.sql.Statement

@Transactional
class NewsService {
    private static Logger logger = LoggerFactory.getLogger(NewsService.class)

    def getAllNews() {
        Connection conn
        List news = new ArrayList()
        Statement stmt
        try {
            Class.forName("com.mysql.jdbc.Driver")
            String url = "jdbc:mysql://host.docker.internal:8889/test"
            String userDb = "tung"
            String pwdDb = "zi5KKnrdHiRKGnR"
            Properties properties = new Properties()
            properties.put("user", userDb)
            properties.put("password", pwdDb)
            conn = DriverManager.getConnection(url, properties)
            stmt = conn.createStatement()
            String sql = "SELECT * FROM news"
            ResultSet result = stmt.executeQuery(sql)
            while (result.next()) {
                news.add result.getString("content")
            }
        } catch (Exception e) {
            logger.error(e.message)
        } finally {
            stmt.close()
            conn.close()
        }
        news
    }
}
