<%--
  Created by IntelliJ IDEA.
  User: tnguyen
  Date: 09/05/2020
  Time: 18:12
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>${title}</title>
</head>

<body>
    <h2>All News</h2>
    <g:each in="${news}" var="item">
        <p>${item}</p>
    </g:each>
</body>
</html>
