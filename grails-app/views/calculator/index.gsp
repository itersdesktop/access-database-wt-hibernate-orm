<%--
  Created by IntelliJ IDEA.
  User: tnguyen
  Date: 20/01/2020
  Time: 14:37
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Calculator</title>
</head>

<body>
    <h2>Scientific Calculator</h2>
    <g:form action="save" useToken="true" name="calculator" method="POST">
        <label>
            Name: <input type="text">
        </label>
        <input type="submit" value="Save" name="btnSave">
    </g:form>
</body>
</html>
