<%--
  Created by IntelliJ IDEA.
  User: tnguyen
  Date: 03/05/2020
  Time: 08:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>List of all articles</title>
</head>

<body>
    <h2>List of all articles: ${nbArticles}</h2>
</body>
</html>
