## How to test database connection without using Hibernate ORM ##
You will likely encounter a headache problem when bringing a large project to cloud based deployment development, 
especially testing the access of databases. This sample project aims to test two approaches of the testing database
 connection. The first approach is to use Hibernate native SQL queries via 
 [DataSource](https://grails.github.io/grails2-doc/2.5.6/guide/conf.html#dataSource) 
 while the second one will play with the plain 
 [JDBC](https://docs.oracle.com/javase/tutorial/jdbc/basics/index.html).
 
**Disclaimer**: This project is built on 
[Grails 2.5.6](https://grails.github.io/grails2-doc/2.5.6/guide/index.html). I am not a hundred percent sure it will
 still work in other versions of Grails.

### Using Hibernate Native SQL Query ###

Please take a look at `ArticleService` where we leverage `SessionFactory` to create SQL queries and extract the
 results. I have stolen the idea of Hubert Klein Ikkink in 
 [his post on DZone](https://dzone.com/articles/grails-goodness-using) claiming that using Native SQL query is
  inevitable.

### Using Plain JDBC ###

Please have a go at the service `NewsService` where we define `Connection` and `Statement` object, then call the
 `executeQuery` method to obtain `ResultSet`. Iterate on this result set to extract data we are needing. You're
  entirely free to take a look at the references I have learned about this approach such as 
  [this post on Grails Asia](http://grails.asia/grails-tutorial-for-beginners-introduction-to-gorm), 
  [Using try-with-resources with JDBC objects](https://blogs.oracle.com/weblogicserver/using-try-with-resources-with-jdbc-objects) or 
  [Lesson: JDBC Basics](https://docs.oracle.com/javase/tutorial/jdbc/basics/index.html).
  
This is the illustrative project for my technical blog post 
[How to test database connection without using Hibernate
 ORM in Grails](https://www.itersdesktop.com/2020/05/10/how-to-test-database-connection-without-using-hibernate-orm-in-grails/).
  
Have fun with my post!
